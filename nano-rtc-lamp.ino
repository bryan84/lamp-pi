// Date and time functions using a DS3231 RTC connected via I2C and Wire lib
#include "RTClib.h"
RTC_DS3231 rtc;

//char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
//now.dayOfTheWeek() returns values 0-6, sunday-saturday

//pin assignments
byte light1 = 2;
byte light2 = 3;
byte dst_switch = 4;
byte lightswitch1 = 5;
byte lightswitch2 = 6;

//flag for a section of the program in the while loop that should only run once when the program starts
byte run_once_flag = 0;

//variables for loops
unsigned long previousMillis = 0;
const long interval = 25000; //25 seconds ensures it runs at least once per minute
unsigned long previousMillis2 = 0;
const long interval2 = 65000; //65 seconds ensures it only runs once at least a full minute has passed

byte light_state_1;
byte switch_state_1;
byte last_switch_state_1;
byte off_flag_1 = 0; //off_flags are used to make sure that the light doesn't turn on automatically if it was turned off manually. they temporarily disable the auto-on functions
byte light_state_2;
byte switch_state_2 = 1;
byte last_switch_state_2 = 1;
byte off_flag_2 = 0; 

int day_number; //needs to be int because it could be a value up to 367
//make sure "off_minute" variables have values, in case program starts after sunset/sunrise 
byte on_hour_sunset;
signed int on_minute_sunset; //signed because the value might be negative
byte off_hour_sunset;
long off_minute_sunset = 15; //needs to be long because the random number generator used later needs type long
byte off_hour_sunrise = 9; //declaring this variable here in case delaring it in every loop might cause problems
byte off_minute_sunrise = 15;
//set on time before sunrise. these values are not changed by the program and are only set here: 
byte on_hour_sunrise = 7; //a value is set here but is changed by the program only for DST corrections
byte on_minute_sunrise = 0; //00

//actual sunset/sunrise times
//use this line for the reduced memory option found in the main loop:
byte sunset_hours;
//use this line for standard time:
//byte sunset_hours[] = {17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17};
//use the line for daylight savings time (DST):
//byte sunset_hours[] = {18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18};
byte sunset_minutes[] = {38,39,39,40,41,42,42,43,44,45,45,46,47,48,48,49,50,51,52,52,53,54,55,55,56,57,58,59,59,0,1,2,2,3,4,5,5,6,7,8,8,9,10,10,11,12,12,13,14,14,15,16,16,17,18,18,19,19,20,21,21,22,22,23,23,24,25,25,26,26,27,27,28,28,29,29,29,30,31,31,32,32,33,33,34,34,35,35,36,36,37,37,38,38,39,39,40,40,41,41,42,42,43,43,44,44,45,45,46,46,47,47,48,48,49,49,50,50,51,51,52,53,53,54,54,55,55,56,56,57,58,58,59,59,0,0,1,1,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,11,12,12,13,13,13,14,14,14,15,15,15,16,16,16,16,16,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,16,16,16,16,15,15,15,14,14,14,13,13,12,12,12,11,10,10,9,9,8,8,7,6,6,5,4,3,3,2,1,0,59,59,58,57,56,55,54,53,52,51,50,49,48,47,46,45,44,43,42,41,40,39,38,37,36,35,34,32,31,30,29,28,27,26,25,23,22,21,20,19,18,17,16,14,13,12,11,10,9,8,7,5,4,3,2,1,0,59,58,57,56,55,54,53,52,51,50,49,48,47,46,45,44,44,43,42,41,40,40,39,38,37,37,36,35,35,34,33,33,32,32,31,31,30,30,29,29,29,28,28,28,27,27,27,27,27,27,26,26,26,26,26,26,26,26,27,27,27,27,27,27,28,28,28,29,29,29,30,30,31,31,31,32,33,33,34,34,35,35,36,37,37,38,38};
//use this line for the reduced memory option found in the main loop:
byte sunrise_hours;
//use this line for standard time:
//byte sunrise_hours[] = {7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7};
//use this line for DST:
//byte sunrise_hours[] = {8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8};
byte sunrise_minutes[] = {8,9,9,9,9,9,10,10,10,10,10,10,10,10,10,10,10,9,9,9,9,9,8,8,8,8,7,7,6,6,6,5,5,4,3,3,2,2,1,0,0,59,58,58,57,56,55,55,54,53,52,51,50,50,49,48,47,46,45,44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,29,27,26,25,24,23,22,21,20,19,18,17,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,59,58,57,56,55,55,54,53,52,51,50,49,48,47,46,45,44,44,43,42,41,40,40,39,38,38,37,36,36,35,34,34,33,33,32,32,31,31,30,30,29,29,29,28,28,28,27,27,27,27,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,27,27,27,27,28,28,28,28,29,29,29,30,30,30,31,31,32,32,32,33,33,34,34,35,35,36,36,37,37,38,38,39,39,40,40,41,41,42,42,43,43,44,44,45,45,46,46,47,47,48,48,49,49,50,50,51,51,52,52,53,53,54,54,55,55,56,56,56,57,57,58,58,59,59,0,0,0,1,1,2,2,3,3,3,4,4,5,5,5,6,6,7,7,8,8,8,9,9,10,10,11,11,12,12,12,13,13,14,14,15,15,16,16,17,17,18,18,19,19,20,21,21,22,22,23,23,24,25,25,26,26,27,28,28,29,30,30,31,32,32,33,34,34,35,36,37,37,38,39,40,40,41,42,42,43,44,45,46,46,47,48,49,49,50,51,51,52,53,54,54,55,56,56,57,58,58,59,0,0,1,2,2,3,3,4,4,5,5,6,6,6,7,7,8,8,8,8};

byte all_night_on_hour;
byte all_night_on_minute;
byte all_night_off_hour;
byte all_night_off_minute;

void setup () {
  pinMode(light1, OUTPUT);
  pinMode(light2, OUTPUT);
  //switches use internal pullup resistors so the logic is inverted
  pinMode(dst_switch, INPUT_PULLUP); //HIGH for standard/winter time, LOW for savings/summer time
  pinMode(lightswitch1, INPUT_PULLUP);
  pinMode(lightswitch2, INPUT_PULLUP);

  //initialize values for switch_state and last_switch_state to determine the current state of the switches
  //prevents lights from coming on when the program starts regardless of switch position
  if (digitalRead(lightswitch1) == LOW) {
    switch_state_1 = 0;
    last_switch_state_1 = 0;
  }
  else {
    switch_state_1 = 1;
    last_switch_state_1 = 1;
  }
  if (digitalRead(lightswitch2) == LOW) {
    switch_state_2 = 0;
    last_switch_state_2 = 0;
  }
  else {
    switch_state_2 = 1;
    last_switch_state_2 = 1;
  }

  #ifndef ESP8266
    while (!Serial); // for Leonardo/Micro/Zero
  #endif
  Serial.begin(9600);
  delay(3000); // wait for console opening

  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }

  if (rtc.lostPower()) {
    Serial.println("RTC lost power, lets set the time!");
    // If the RTC have lost power it will sets the RTC to the date & time this sketch was compiled in the following line
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }
  
  // If you need to set the time of the uncomment line 34 or 37
  // following line sets the RTC to the date & time this sketch was compiled
  // rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  // This line sets the RTC with an explicit date & time, for example to set
  // January 21, 2014 at 3am you would call:
  // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));

  //because of a PNP transistor on the relay driver circuit, writing HIGH to pins turns the lights off.
  //this turns the lamp off at the beginning of the program
  digitalWrite(light1, HIGH);
  digitalWrite(light2, HIGH);
}

void loop () {
  //switches toggle lamp states
  switch_state_1 = digitalRead(lightswitch1);
  //compare the current state with the previous state
  if (switch_state_1 != last_switch_state_1) {
    if (light_state_1 == LOW) {
      light1_on();
      off_flag_1 = 0;
    }
    else {
      light1_off();
      off_flag_1 = 1;
    }
    delay(20);
    last_switch_state_1 = switch_state_1;
  }
  switch_state_2 = digitalRead(lightswitch2);
  //compare the current state with the previous state
  if (switch_state_2 != last_switch_state_2) {
    if (light_state_2 == LOW) {
      light2_on();
      off_flag_2 = 0;
    }
    else {
      light2_off();
      off_flag_2 = 1;
    }
    delay(20);
    last_switch_state_2 = switch_state_2;
  }
  
  //"blink without delay" if statement, allows microcontroller to do other things (like toggle lamp states) in between figuring out what time it is
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    // save the time
    previousMillis = currentMillis;

    //set the time
    DateTime now = rtc.now();
  
    //convert rtc month/day to a day-of-year number
    day_number = calculate_day_number(now.month(), now.day());

    //set on_hour_sunset in a way that doesn't use so much memory, but increases processor demands
    //FIND A WAY TO DO THIS ONCE PER DAY and also when the program first starts
    on_hour_sunset = calculate_sunset_hour();
    off_hour_sunrise = calculate_sunrise_hour();
    on_hour_sunrise = calculate_sunrise_hour_on(); //this is just a DST consideration but needs to be done first

    //sets sunset/sunrise times and delays for inside lamp
    //on/off times for second light which stays on from sunset to sunrise
    all_night_on_hour = on_hour_sunset;
    all_night_off_hour = off_hour_sunrise;
    all_night_on_minute = sunset_minutes[day_number]; //sunset-to-sunrise on minute set
    all_night_off_minute = sunrise_minutes[day_number]; //sunset-to-sunrise off minute set

    //subtract 30 minutes from sunset time since it gets dark before actual sunset
    //on_hour_sunset = sunset_hours[day_number]; //comment out when using reduced memory "if" statements above
    on_minute_sunset = sunset_minutes[day_number] - 30;
    if (on_minute_sunset < 0) {
      on_hour_sunset = on_hour_sunset - 1;
      on_minute_sunset = on_minute_sunset + 60;
    }
    //add some time to the sunrise time for a similar reason
    //off_hour_sunrise = sunrise_hours[day_number];   //comment out when using reduced memory "if" statements above
    off_minute_sunrise = sunrise_minutes[day_number] + 30;
    if (off_minute_sunrise > 59) {
      off_hour_sunrise = off_hour_sunrise + 1;
      off_minute_sunrise = off_minute_sunrise - 60;
    }

    if (off_flag_1 == 0) { //make sure the lamp wasn't just turned off manually, this will prevent it from coming right back on
      //turn on the lamp
      if (now.hour() == on_hour_sunset and now.minute() == on_minute_sunset) {
        light1_on();
        //use this opportunity to calculate a random time for the light to turn off
        //placement here insures random int is only generated once per day
        off_minute_sunset = random(1,59);
      }
      //ensures that the lamp doesn't turn on after the turn-off time in the mornings
      //this could be a problem in the summer time when the sun comes up early, and might leave the lamp on all day for no reason
      if (now.dayOfTheWeek() != 0 and now.dayOfTheWeek() != 6) { //saturday/sunday exception
        if ((now.hour() == on_hour_sunrise and now.minute() == on_minute_sunrise)) {
          if (off_hour_sunrise > on_hour_sunrise) {
            light1_on();
          }
          if ((off_hour_sunrise == on_hour_sunrise) and (off_minute_sunrise > (on_minute_sunrise + 10))) {
            light1_on();
          }
        }
      }
    }
    
    //determine appropriate time to turn lamp off (later on weekends), this is affected by DST changes because the system time doesn't change at DST
    //this is in the loop and not at the top because it needs to change based on the day
    if (now.dayOfTheWeek() == 6 or now.dayOfTheWeek() == 0 or digitalRead(dst_switch) == LOW) { //added dst here to keep the lamp on longer in winter
      //this isn't set at 0/24 because the math/programming gets more complicated than i want. 11/10 standard/dst is late enough i suppose
      off_hour_sunset = 23; //11:00, later on weekends and during standard time
    }
    else {
      off_hour_sunset = 22;
    }

    //turn the lamp on/off if the program starts at a time when the lamp should be on
    //run this part only one time using the "run_once_flag" variable
    if (run_once_flag == 0) {
      if ((now.hour() == on_hour_sunset and now.minute() >= on_minute_sunset) or (now.hour() > on_hour_sunset)) {
        if ((now.hour() == off_hour_sunset and now.minute() < off_minute_sunset) or (now.hour() < off_hour_sunset)) {
          light1_on();
        }
      }
      if (now.dayOfTheWeek() != 0 and now.dayOfTheWeek() != 6) { //Sat/Sun exception
        if ((now.hour() == on_hour_sunrise and now.minute() >= on_minute_sunrise) or (now.hour() > on_hour_sunrise)) {
          if ((now.hour() == off_hour_sunrise and now.minute() < off_minute_sunrise) or (now.hour() < off_hour_sunrise)) {
            light1_on();
          }
        }
      }
      if ((now.hour() == all_night_on_hour and now.minute() >= all_night_on_minute) or (now.hour() > all_night_on_hour and now.hour() < 23) or (now.hour() == 23 and now.minute() <= 59)) {
        light2_on();
      }
      if ((now.hour() < all_night_off_hour) or (now.hour() == all_night_off_hour and now.minute() < all_night_off_minute)) {
        light2_on();
      }
      run_once_flag = 1;
    }

    //turn off the lamp
    if ((now.hour() == off_hour_sunset and now.minute() == off_minute_sunset) or (now.hour() == off_hour_sunrise and now.minute() == off_minute_sunrise)) {
      light1_off();
      off_flag_1 = 0; //reset the off flag so that it will work again the next time its needed
    }

    //turn on the all night light
    if (off_flag_2 == 0) {
      if (now.hour() == all_night_on_hour and now.minute() == all_night_on_minute) {
        light2_on();
      }
    }
    //turn off the all night light
    if (now.hour() == all_night_off_hour and now.minute() == all_night_off_minute) {
      light2_off();
      off_flag_2 = 0;
    }
  }

  //reset the off_flags after a minute, since the program only turns the lights on during a one-minute interval, the off flags can be cleared after that time
  unsigned long currentMillis2 = millis();
  if (currentMillis - previousMillis >= interval2) {
    previousMillis2 = currentMillis2;
    off_flag_1 = 0;
    off_flag_2 = 0;
  }
}

void light1_on(void) {
  light_state_1 = HIGH;
  digitalWrite(light1, LOW); //PNP transistors on relay driving circuit mean "low" signal turns the lamps on
}

void light1_off(void) {
  light_state_1 = LOW;
  digitalWrite(light1, HIGH);
}

void light2_on(void) {
  light_state_2 = HIGH;
  digitalWrite(light2, LOW);
}

void light2_off(void) {
  light_state_2 = LOW;
  digitalWrite(light2, HIGH);
}

//calculate_ functions read a switch that sets DST vs standard time
int calculate_sunset_hour() {
  if ((day_number <= 29) or (day_number > 280 and day_number <= 367)) {
    on_hour_sunset = 18;
  }
  if ((day_number > 29 and day_number <= 134) or (day_number > 223 and day_number <= 280)) {
    on_hour_sunset = 19;
  }
  if (day_number > 134 and day_number <= 223) {
    on_hour_sunset = 20;
  }
  return on_hour_sunset;
}

int calculate_sunrise_hour() {
  if ((day_number <= 41) or (day_number > 347 and day_number <= 367)) {
    off_hour_sunrise = 8;
  }
  if ((day_number > 41 and day_number <= 101) or (day_number > 244 and day_number <= 347)) {
    off_hour_sunrise = 7;
  }
  if (day_number > 101 and day_number <= 244) {
    off_hour_sunrise = 6;
  }
  return off_hour_sunrise;
}

int calculate_day_number(int month, int day) {
  byte daysInMonth[] = {31,28,31,30,31,30,31,31,30,31,30,31};
  int day_of_year = 0;
  for (int i = 0; i < month -1; i++) {
    day_of_year += daysInMonth[i];
  }
  day_of_year += day;
  return day_of_year;
}

int calculate_sunrise_hour_on() { //to wake up at time X, put X for if statement and X+1 for else
  if (digitalRead(dst_switch) == HIGH) {
    on_hour_sunrise = 7; 
  }
  else {
    on_hour_sunrise = 8;
  }
  return on_hour_sunrise;
}