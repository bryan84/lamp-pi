// Date and time functions using a DS3231 RTC connected via I2C and Wire lib
#include "RTClib.h"
RTC_DS3231 rtc;

//pin assignments
byte light = 2; //for the second switch/light, look at previous revisions to this sketch
byte dst_switch = 4;
byte lightswitch = 5;

//flag for a section of the program in the while loop that should only run once when the program starts
bool run_once_flag = false;

//variables for loops
unsigned long previousMillis = 0;
const long interval = 58000; //58 seconds ensures it runs at least once per minute and doesn't re-turn-on if you turn it off once it turns itself on

byte light_state;
byte switch_state;
byte last_switch_state;

int day_number; //needs to be int because it could be a value up to 367
//make sure "off_minute" variables have values, in case program starts after sunset 
byte on_hour_sunset;
signed int on_minute_sunset; //signed because the value might be negative
byte off_hour_sunset;
long off_minute_sunset = 15; //needs to be long because the random number generator used later needs type long
byte offset = 35; //offset the sunset time by a certain number of minutes

//select whether to use sunset or twilight times by commenting out one or the other.
//you will also need to comment out one or the other calculate_sunset_hour() function
byte sunset_hour;
//sunset times
//byte sunset_minutes[] = {10,11,12,13,14,15,16,18,19,20,21,22,23,25,26,27,28,30,31,32,34,35,36,38,39,40,42,43,44,46,47,49,50,51,53,54,56,57,58,0,1,3,4,5,7,8,10,11,12,14,15,16,18,19,20,22,23,24,26,27,28,30,31,32,34,35,36,37,39,40,41,43,44,45,46,48,49,50,51,53,54,55,56,58,59,0,1,3,4,5,6,7,9,10,11,12,14,15,16,17,18,20,21,22,23,25,26,27,28,30,31,32,33,34,36,37,38,39,41,42,43,44,45,47,48,49,50,51,52,54,55,56,57,58,59,0,1,3,4,5,6,7,8,9,10,11,12,12,13,14,15,16,17,17,18,19,20,20,21,22,22,23,23,24,24,25,25,25,26,26,26,26,26,27,27,27,27,27,27,27,26,26,26,26,26,25,25,24,24,23,23,22,22,21,21,20,19,18,18,17,16,15,14,13,12,11,10,9,8,7,5,4,3,2,0,59,58,56,55,54,52,51,49,48,46,45,43,42,40,38,37,35,33,32,30,28,27,25,23,21,20,18,16,14,13,11,9,7,5,3,2,0,58,56,54,52,50,49,47,45,43,41,39,37,36,34,32,30,28,26,24,23,21,19,17,15,13,12,10,8,6,4,3,1,59,57,56,54,52,51,49,47,46,44,43,41,39,38,36,35,33,32,31,29,28,26,25,24,22,21,20,19,18,17,15,14,13,12,11,10,9,9,8,7,6,5,5,4,4,3,2,2,1,1,1,0,0,0,0,0,59,59,59,59,59,0,0,0,0,0,1,1,1,2,2,3,3,4,5,5,6,7,8,8,9,9};
//civil twilight times
byte sunset_minutes[] = {43,44,45,46,47,48,49,50,51,52,53,54,55,57,58,59,0,1,2,4,5,6,7,9,10,11,13,14,15,16,18,19,20,22,23,24,26,27,28,30,31,32,34,35,36,38,39,40,42,43,44,45,47,48,49,51,52,53,55,56,56,57,58,0,1,2,4,5,6,7,9,10,11,12,14,15,16,18,19,20,21,23,24,25,26,28,29,30,31,33,34,35,37,38,39,40,42,43,44,46,47,48,50,51,52,53,55,56,57,59,0,1,3,4,5,7,8,9,11,12,13,15,16,17,19,20,21,23,24,25,26,28,29,30,32,33,34,35,36,38,39,40,41,42,43,44,46,47,48,49,50,51,51,52,53,54,55,56,56,57,58,58,59,0,0,1,1,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2,1,1,1,0,59,59,58,58,57,56,55,54,53,53,52,51,50,49,47,46,45,44,43,42,40,39,38,36,35,34,32,31,29,28,26,25,23,21,20,18,17,15,13,12,10,8,6,5,3,1,59,57,56,54,52,50,48,47,45,43,41,39,37,35,33,31,30,28,26,24,22,20,18,16,14,12,11,9,7,5,3,1,59,57,56,54,52,50,48,46,45,43,41,39,37,36,34,32,30,29,27,25,24,22,20,19,17,16,14,12,11,9,8,7,5,4,2,1,0,58,57,56,54,53,52,51,50,49,48,47,46,45,44,43,42,41,40,40,39,38,38,37,36,36,35,35,34,34,34,33,33,33,33,33,32,32,32,32,32,33,33,33,33,33,34,34,34,35,35,36,36,37,37,38,39,39,40,41,41,42,42};

void setup () {
  pinMode(light, OUTPUT);
  //switches use internal pullup resistors so the logic is inverted
  pinMode(dst_switch, INPUT_PULLUP); //HIGH for standard/winter time, LOW for savings/summer time
  pinMode(lightswitch, INPUT_PULLUP);
  pinMode(LED_BUILTIN, OUTPUT); //indicator LED for the lamp state

  //initialize values for switch_state and last_switch_state to determine the current state of the switches
  //prevents lights from coming on when the program starts regardless of switch position
  if (digitalRead(lightswitch) == LOW) {
    switch_state = 0;
    last_switch_state = 0;
  }
  else {
    switch_state = 1;
    last_switch_state = 1;
  }

  #ifndef ESP8266
    while (!Serial); // for Leonardo/Micro/Zero
  #endif
  Serial.begin(9600);
  delay(3000); // wait for console opening

  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }

  if (rtc.lostPower()) {
    //NOTE: IF YOU COMPILE THIS SKETCH DURING DST, THE RTC WILL BE SET TO DST!
    Serial.println("RTC lost power, lets set the time!");
    // If the RTC have lost power it will sets the RTC to the date & time this sketch was compiled in the following line
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }

  //because of a PNP transistor on the relay driver circuit, writing HIGH to pins turns the lights off.
  //this turns the lamp off at the beginning of the program
  digitalWrite(light, HIGH);
  delay(2000);
}

void loop () {
  //switches toggle lamp states
  switch_state = digitalRead(lightswitch);
  //compare the current state with the previous state
  if (switch_state != last_switch_state) {
    if (light_state == LOW) {
      light_on();
    }
    else {
      light_off();
    }
    delay(20); //debounce delay
    last_switch_state = switch_state;
  }

  //MIGHT NEED TO ADD A CHECK HERE TO SEE IF THE RTC IS WORKING, ADD IN THE FUTURE POSSIBLY
  //"blink without delay" if statement, allows microcontroller to do other things (like toggle lamp states) in between figuring out what time it is
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    // save the time
    previousMillis = currentMillis;
    //set the time
    DateTime now = rtc.now();
  
    //convert rtc month/day to a day-of-year number
    day_number = calculate_day_number(now.month(), now.day());

    //set on_hour_sunset in a way that doesn't use so much memory, but increases processor demands
    //FIND A WAY TO DO THIS ONCE PER DAY and also when the program first starts
    on_hour_sunset = calculate_sunset_hour();

    //subtract "offset" number of minutes from sunset time since it gets dark before actual sunset
    //use this section for using natural sunset times, use the next section for using twilight times
    
    on_minute_sunset = sunset_minutes[day_number] - offset;
    if (on_minute_sunset < 0) {
      on_hour_sunset = on_hour_sunset - 1;
      on_minute_sunset = on_minute_sunset + 60;
    } 

    //turn on the lamp
    if (now.hour() == on_hour_sunset and now.minute() == on_minute_sunset) {
      light_on();
      //use this opportunity to calculate a random time for the light to turn off, placement here insures random int is only generated once per day
      off_minute_sunset = random(1,59);
    }
    
    //DST adjustments to keep the lamp turning off at the same time, also turns off later on weekends
    //when the RTC says its 10:00 pm in DST the DST time is 11:00, so reduce the hour by 1
    if (digitalRead(dst_switch) == HIGH) { //HIGH is standard/winter time
      if (now.dayOfTheWeek() == 6 or now.dayOfTheWeek() == 0) {
        off_hour_sunset = 23;
      }
      else {
        off_hour_sunset = 22;
      }
    }
    else { //LOW is summer/dst
      if (now.dayOfTheWeek() == 6 or now.dayOfTheWeek() == 0) { //might need to change this to offset by one day in case 23:00 in standard time is 0:00 in dst which is the next day
        off_hour_sunset = 22;
      }
      else {
        off_hour_sunset = 21;
      }
    }

    //turn the lamp on/off if the program starts at a time when the lamp should be on
    //run this part only one time using the "run_once_flag" variable
    if (run_once_flag == false) {
      if ((now.hour() == on_hour_sunset and now.minute() >= on_minute_sunset) or (now.hour() > on_hour_sunset)) {
        if ((now.hour() == off_hour_sunset and now.minute() < off_minute_sunset) or (now.hour() < off_hour_sunset)) {
          light_on();
        }
      }
      run_once_flag = true;
    }

    //turn off the lamp
    if (now.hour() == off_hour_sunset and now.minute() == off_minute_sunset) {
      light_off();
    }
  }
}

void light_on(void) {
  light_state = HIGH;
  digitalWrite(light, LOW); //PNP transistors on relay driving circuit mean "low" signal turns the lamps on
  digitalWrite(LED_BUILTIN, HIGH);
}

void light_off(void) {
  light_state = LOW;
  digitalWrite(light, HIGH);
  digitalWrite(LED_BUILTIN, LOW);
}

//twilight hours
int calculate_sunset_hour() {
  if ((day_number <= 15) or (day_number > 304 and day_number <=367)) { //changed 16 to 15 here to account for day 0 in array
    sunset_hour = 16;
  }
  if ((day_number > 15 and day_number <= 62) or (day_number > 267 and day_number <= 304)) {
    sunset_hour = 17;
  }
  if ((day_number > 62 and day_number <= 109) or (day_number > 235 and day_number <= 267)) {
    sunset_hour = 18;
  }
  if ((day_number > 109 and day_number <= 162) or (day_number > 189 and day_number <= 235)) {
    sunset_hour = 19;
  }
  if (day_number > 162 and day_number <= 189) {
    sunset_hour = 20;
  }
  return sunset_hour;
}

//sunset hours 
/*
int calculate_sunset_hour() { //added +1 to each line here
  if ((day_number <= 39) or (day_number > 284 and day_number <= 340) or (day_number > 345 and day_number <= 366)) {
    on_hour_sunset = 17;
  }
  if ((day_number > 39 and day_number <= 85) or (day_number > 252 and day_number <= 284)) {
    on_hour_sunset = 18;
  }
  if ((day_number > 85 and day_number <= 135) or (day_number > 215 and day_number <= 252)) {
    on_hour_sunset = 19;
  }
  if (day_number > 135 and day_number <= 215) {
    on_hour_sunset = 20;
  }
  if (day_number > 340 and day_number <= 345) {
    on_hour_sunset = 16;
  }
  return on_hour_sunset;
}
*/

int calculate_day_number(int month, int day) {
  byte daysInMonth[] = {31,28,31,30,31,30,31,31,30,31,30,31};
  int day_of_year = 0;
  for (int i = 0; i < month -1; i++) {
    day_of_year += daysInMonth[i];
  }
  day_of_year += day;
  return day_of_year;
}
