import time
from datetime import datetime
import random
import RPi.GPIO as GPIO

output_pin = 12
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(output_pin, GPIO.OUT)
#flag for a section of the program in the while loop that should only run once when the program starts
run_once_flag = 0

#make sure "off_minute" have values, in case program starts after sunset/sunrise
off_minute_sunset = 15
off_minute_sunrise = 15
#set on time before sunrise. these values are not changed by the program and are only set here:
on_hour_sunrise = 7
on_minute_sunrise = 0

#actual sunset/sunrise times
sunset_hours = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
sunset_minutes = [38,39,39,40,41,42,42,43,44,45,45,46,47,48,48,49,50,51,52,52,53,54,55,55,56,57,58,59,59,0,1,2,2,3,4,5,5,6,7,8,8,9,10,10,11,12,12,13,14,14,15,16,16,17,18,18,19,19,20,21,21,22,22,23,23,24,25,25,26,26,27,27,28,28,29,29,29,30,31,31,32,32,33,33,34,34,35,35,36,36,37,37,38,38,39,39,40,40,41,41,42,42,43,43,44,44,45,45,46,46,47,47,48,48,49,49,50,50,51,51,52,53,53,54,54,55,55,56,56,57,58,58,59,59,0,0,1,1,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,11,12,12,13,13,13,14,14,14,15,15,15,16,16,16,16,16,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,16,16,16,16,15,15,15,14,14,14,13,13,12,12,12,11,10,10,9,9,8,8,7,6,6,5,4,3,3,2,1,0,59,59,58,57,56,55,54,53,52,51,50,49,48,47,46,45,44,43,42,41,40,39,38,37,36,35,34,32,31,30,29,28,27,26,25,23,22,21,20,19,18,17,16,14,13,12,11,10,9,8,7,5,4,3,2,1,0,59,58,57,56,55,54,53,52,51,50,49,48,47,46,45,44,44,43,42,41,40,40,39,38,37,37,36,35,35,34,33,33,32,32,31,31,30,30,29,29,29,28,28,28,27,27,27,27,27,27,26,26,26,26,26,26,26,26,27,27,27,27,27,27,28,28,28,29,29,29,30,30,31,31,31,32,33,33,34,34,35,35,36,37,37,38,38]
sunrise_hours = [7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7]
sunrise_minutes = [8,9,9,9,9,9,10,10,10,10,10,10,10,10,10,10,10,9,9,9,9,9,8,8,8,8,7,7,6,6,6,5,5,4,3,3,2,2,1,0,0,59,58,58,57,56,55,55,54,53,52,51,50,50,49,48,47,46,45,44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,29,27,26,25,24,23,22,21,20,19,18,17,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,59,58,57,56,55,55,54,53,52,51,50,49,48,47,46,45,44,44,43,42,41,40,40,39,38,38,37,36,36,35,34,34,33,33,32,32,31,31,30,30,29,29,29,28,28,28,27,27,27,27,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,27,27,27,27,28,28,28,28,29,29,29,30,30,30,31,31,32,32,32,33,33,34,34,35,35,36,36,37,37,38,38,39,39,40,40,41,41,42,42,43,43,44,44,45,45,46,46,47,47,48,48,49,49,50,50,51,51,52,52,53,53,54,54,55,55,56,56,56,57,57,58,58,59,59,0,0,0,1,1,2,2,3,3,3,4,4,5,5,5,6,6,7,7,8,8,8,9,9,10,10,11,11,12,12,12,13,13,14,14,15,15,16,16,17,17,18,18,19,19,20,21,21,22,22,23,23,24,25,25,26,26,27,28,28,29,30,30,31,32,32,33,34,34,35,36,37,37,38,39,40,40,41,42,42,43,44,45,46,46,47,48,49,49,50,51,51,52,53,54,54,55,56,56,57,58,58,59,0,0,1,2,2,3,3,4,4,5,5,6,6,6,7,7,8,8,8,8]

#convert sunset_hours to 24-hour format
for i in range(len(sunset_hours)):
	sunset_hours[i] = sunset_hours[i] + 12

#DST:
for i in range(len(sunset_hours)):
	sunset_hours[i] = sunset_hours[i] + 1
for i in range(len(sunrise_hours)):
	sunrise_hours[i] = sunrise_hours[i] + 1

while 1:
	#save the day number (0-366) as an int
	day_number = datetime.now().timetuple().tm_yday
	#figure out the current time
	now_hour = time.localtime(time.time())[3]
	now_minute = time.localtime(time.time())[4]
	#day_of_week returns 0-6 depending on day of week
	day_of_week = datetime.now().weekday()

	#subtract 30 minutes from sunset time since it gets dark before actual sunset
	on_hour_sunset = sunset_hours[day_number]
	on_minute_sunset = sunset_minutes[day_number] - 30
	if on_minute_sunset < 0:
		on_hour_sunset = on_hour_sunset - 1
		on_minute_sunset = on_minute_sunset + 60
	#add 30 minutes to sunrise time for similar reason:
	off_hour_sunrise = sunrise_hours[day_number]
	off_minute_sunrise = sunrise_minutes[day_number] + 30
	if off_minute_sunrise > 59:
		off_hour_sunrise = off_hour_sunrise + 1
		off_minute_sunrise = off_minute_sunrise - 60

	if now_hour == on_hour_sunset and now_minute == on_minute_sunset:
		GPIO.output(output_pin, True)
		#use this opportunity to calculate a random time for the light to turn off
		#placement here insures random int is only calculated once per day
		off_minute_sunset = random.randint(0,59)
	#ensures that the lamp doesn't turn on after the turn-off time in the mornings
	#this could be a problem in the summer time when the sun comes up early, and might leave the lamp on all day for no reason
	if (now_hour == on_hour_sunrise and now_minute == on_minute_sunrise) and (day_of_week !=5 and day_of_week !=6):
		if off_hour_sunrise > on_hour_sunrise:
			GPIO.output(output_pin, True)
		if off_hour_sunrise == on_hour_sunrise and off_minute_sunrise > (on_minute_sunrise + 10):
			GPIO.output(output_pin, True)

	#determine appropriate time to turn lamp off (later on weekends), this is not affected by DST changes because it checks the system clock (i think)
	if day_of_week == 5 or day_of_week == 6:
		off_hour_sunset = 22 #used to be 23
	else:
		off_hour_sunset = 22

	#run this part only one time using the "run_once_flag" variable
	#turn the lamp on/off if the program starts at a time when the lamp should be on
	if run_once_flag == 0:
		if (now_hour == on_hour_sunset and now_minute >= on_minute_sunset) or (now_hour > on_hour_sunset):
			if (now_hour == off_hour_sunset and now_minute < off_minute_sunset) or (now_hour < off_hour_sunset):
				GPIO.output(output_pin, True)
		if (now_hour == on_hour_sunrise and now_minute >= on_minute_sunrise) or (now_hour > on_hour_sunrise):
			if (now_hour == off_hour_sunrise and now_minute < off_minute_sunrise) or (now_hour < off_hour_sunrise):
				GPIO.output(output_pin, True)
		run_once_flag = 1

	#turn off the lamp
	if ((now_hour == off_hour_sunset and now_minute == off_minute_sunset) or (now_hour == off_hour_sunrise and now_minute == off_minute_sunrise)):
		GPIO.output(output_pin, False)

#	print now_hour, now_minute, on_hour_sunset, on_minute_sunset, off_hour_sunset, off_hour_sunset, on_hour_sunrise, on_minute_sunrise, off_hour_sunrise, off_minute_sunrise

	time.sleep(30)